@extends('admin.layouts.layout')
@section('content')
    <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Add|Edit User</span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="/" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                        <a href="#" class="breadcrumb-item">Users</a>
                        <span class="breadcrumb-item active">Add|Edit Users</span>
                    </div>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="card">
                <div class="card-body">
                    <form action="/admin/users/add" method="post" id="booking-form" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <fieldset class="mb-3">
                            <legend class="text-uppercase font-size-sm font-weight-bold">Add|Edit User</legend>
                            <input type="hidden" class="form-control" name="id" value="{{$user->id or ''}}">
                            @include('flash')
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">First Name</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" autocomplete="off" name="first_name" value="{{$user->first_name or ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">Last Name</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" autocomplete="off" name="last_name" value="{{$user->last_name or ''}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">Email</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" autocomplete="off" name="email" value="{{$user->email or ''}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">Old Password</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" autocomplete="off" name="current_password" placeholder="********">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">New Password</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" autocomplete="off" name="new_password" placeholder="********">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">Confirm Password</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" autocomplete="off" name="confirm_password" placeholder="********">
                                </div>
                            </div>
                        </fieldset>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection