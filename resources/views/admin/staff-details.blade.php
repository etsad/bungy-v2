@extends('admin.layouts.layout')
@section('content')
  <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - User</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>

        <div class="content">
        <div class="heading-text">
        <h4>Add New Category</h4>
        </div>
        <div class="export-content">
            <form action="" class="form-wrapper">
                <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                    <label for="">Enter New Category</label>
                        <input type="text" class="form-control" placeholder="">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <button class="btn btn-primary">Add New Category</button>
                    </div>   
                </div>
                </div>
            </form>
        </div>
        </div>


        <div class="content">
            <div class="booking-content">
                <div class="new-booking-wrapper">
                    <h4>  List of Staffs </h4>
                        <div class="card">
                                <form action="" class="form-wrapper">
                                    <div class="form-group">
                                        <input type="text" placeholder="Search by  Name , Staff ID , Email" class="form-control">
                                    </div>
                                </form>
                                 <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Staff Id</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Staff Category</th>
                                                    <th>Contact</th>
                                                    <th class="text-center" style="width: 30px;">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#B091</td>
                                                    <td>Biswas Ghale</td>
                                                    <td><a href="#">ghale@bungynepaladventure.com</a></td>
                                                    <td>Marketing Officer</td>
                                                    <td> 
                                                        <span class="badge badge-info">9808884693</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/edit-staff" class="dropdown-item"><i class="icon-pencil"></i> View & Edit Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                                
                                                
                                            </tbody>
                                        </table>
                                </div>
                                
                        </div>
                        <div class="col-md-12 offset-6"></div>
                </div>
            </div>
        </div>
    </div>


</div>



@endsection