@extends('admin.layouts.layout')
@section('content')
  <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Staff</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>




        <div class="content">
            <div class="booking-content">
                <div class="new-booking-wrapper">
                    <h4>Add New Staff</h4>
                    <div class="card">
                        <div class="form-wrapper">
                            <form action="">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">First Name</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                    <div class="form-group">
                                            <label for="">Last Name</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                    <div class="form-group">
                                            <label for="">Citizenship Id</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="">Personal Email Address</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                    <div class="form-group">
                                            <label for="">Personal Contact Number</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                    <div class="form-group">
                                            <label for="">Living Address </label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                <div class="col-md-4">
                                <div class="form-group">
                                        <label for="">Staff Category</label>
                                        <select name="" id="" class="form-control">
                                            <option value="">Please select category</option>
                                            <option value="">Bungy Staff</option>
                                            <option value="">Security</option>
                                            <option value="">Administrator</option>
                                            <option value="">Vechile Staff</option>
                                            <option value="">Assistant Staff</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                            <label for="">Company Email address</label>
                                            <input type="text" class="form-control">
                                            
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="">Upload User Image</label>
                                        <input type="file" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6 uploaded-staff-image">
                                        <img src="/admin-assets/images/demo/users/face11.jpg" alt="" class="">
                                    </div>
                                </div>
                                <div class="form-group add-staff-btn">
                                <button class="btn btn-success btn-add">Add Staff</button>
                                </div>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>



@endsection