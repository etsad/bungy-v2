@extends('admin.layouts.layout')
@section('content')
  <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Media Upload</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="booking-content">
                <div class="new-booking-wrapper">
                    <h4>Add New Media ( IMAGES )</h4>
                    <div class="card">
                        <div class="media-upload-wrapper">
                            <div class="form-wrapper">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                        <div class="">
                                            <img class="new-img-preview" src="https://via.placeholder.com/200" title="">
                                        </div>
                                        <div class="input-group">
                                            <div class="input-group-btn">
                                            <div class="fileUpload btn btn-default btn-lg fake-shadow">
                                                <span><i class="glyphicon glyphicon-upload"></i> Select Image</span>
                                                <input id="new-img-id" name="logo" type="file" class="attachment_upload">
                                            </div>
                                            </div>
                                        </div>
                                        </div>  
                                    </div>
                                    <div class="col-md-4">
                                    
                                    </div>
                                    <div class="col-md-4">
                                    
                                    </div>
                                </div>
                                
                            </div>
                            <div class="ancor-division">
                                    <a href=""><span class="pull-right icon-arrow-right16"></span>  SAVE & UPLOAD ALL </a>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="booking-content">
                <div class="new-booking-wrapper">
                    <h4>Add New Media ( Video )</h4>
                    <div class="card">
                        <div class="media-upload-wrapper">
                            <div class="form-wrapper">
                                <div class="form-group">
                                    <label for="">Paste video url here ( Youtube )</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Title of video</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Add Description</label>
                                    <textarea name="" id="" cols="30" rows="10" class="form-control text-box"></textarea>
                                </div>
                            </div>
                            <div class="ancor-division">
                                    <a href=""><span class="pull-right icon-arrow-right16"></span>  SAVE & PUBLISH VIDEO </a>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




    </div>


</div>


<script>
$(document).ready(function() {
    var brand = document.getElementById('new-img-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.new-img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#new-img-id").change(function() {
        readURL(this);
    });
});
</script>
@endsection