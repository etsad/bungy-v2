@extends('admin.layouts.layout')
@section('content')
    <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">View Booking - Booking Id: #{{$booking->ref_id}}</span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="card">
                <div class="card-body">
                        <fieldset class="mb-3">
                            <legend class="text-uppercase font-size-sm font-weight-bold">View Booking</legend>
                            <input type="hidden" class="form-control" disabled value="{{$booking->id}}">
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">First Name</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" disabled value="{{$booking->first_name}}">
                                </div>
                                <label class="col-form-label font-weight-bold col-lg-2">Last Name</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" disabled value="{{$booking->last_name}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">Email</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" disabled value="{{$booking->email}}">
                                </div>
                                <label class="col-form-label font-weight-bold col-lg-2">Phone</label>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" disabled value="{{$booking->phone}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">Gender</label>
                                <div class="col-lg-4">
                                    <select  disabled class="form-control">
                                        <option @if($booking->gender == "Male") selected @endif value="Male">Male</option>
                                        <option @if($booking->gender == "Female") selected @endif value="Female">Female</option>
                                    </select>
                                </div>
                                <label class="col-form-label font-weight-bold col-lg-2">Blood Group</label>
                                <div class="col-lg-4">
                                    <select  disabled class="form-control">
                                        <option @if($booking->blood_group == "A+") selected @endif value="A+">A+</option>
                                        <option @if($booking->blood_group == "A-") selected @endif value="A-">A-</option>
                                        <option @if($booking->blood_group == "B+") selected @endif value="B+">B+</option>
                                        <option @if($booking->blood_group == "B-") selected @endif value="B-">B-</option>
                                        <option @if($booking->blood_group == "O-") selected @endif value="O-">O-</option>
                                        <option @if($booking->blood_group == "O+") selected @endif value="O+">O+</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">Jump Date</label>
                                <div class="col-lg-4">
                                    <input id="today" autocomplete="off" disabled type="text" class="form-control datepicker" value="{{$booking->jump_date}}">
                                </div>
                                <label class="col-form-label font-weight-bold col-lg-2">Number of Jumpers</label>
                                <div class="col-lg-4">
                                    <input type="text" disabled class="form-control" placeholder="Number of Jumpers" value="{{$booking->quantity}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">Age</label>
                                <div class="col-lg-4">
                                    <input disabled type="text" class="form-control" value="{{$booking->age}}">
                                </div>
                                <label class="col-form-label font-weight-bold col-lg-2">Nationality</label>
                                <div class="col-lg-4">
                                    <select  disabled class="form-control">
                                    <option value="Foreigner" @if($booking->user_type == "Foreigner") selected @endif>Foreigner</option>
                                    <option value="Nepalese" @if($booking->user_type == "Nepalese") selected @endif>Nepalese</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">Citizenship/Passport Number</label>
                                <div class="col-lg-4">
                                    <input type="text" disabled class="form-control"  value="{{$booking->citizenship_number}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-form-label font-weight-bold col-lg-2">Citizenship/Passport Image</label>
                                <div class="col-lg-4">
                                    <img src="/uploads/citizenship/{{$booking->citizenship_front}}" width="100" class="img-responsive">
                                </div>
                            </div>
                        </fieldset>
                        <div class="text-right">
                            <a href="/admin/booking"  class="btn btn-primary">Back<i class="icon-arrow-left7 ml-2"></i></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection