@extends('admin.layouts.layout')
@section('content')
    <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Manage Booking Lists</span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>

        <div class="content">
        <div class="heading-text">
        <h4>Quickly Export Completed Bookings</h4>
        </div>
        <div class="export-content">
            <form action="" class="form-wrapper">
                <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">From Date</label>
                        <input type="date" class="form-control"  placeholder="Enter Date">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">To Date</label>
                        <input type="date" class="form-control"  placeholder="Enter Date">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <button class="btn btn-success">Export to Excel</button>
                    </div>   
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <button class="btn btn-danger">Export to PDF</button>
                    </div>   
                </div>
                </div>
            </form>
            <div class="card">
                                 <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Booking Id</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Jump Date</th>
                                                    <th>Status</th>
                                                    <th class="text-center" style="width: 30px;">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-success">Completed</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-eye"></i> View</a>
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to approve this booking ?'))" class="dropdown-item"><i class="icon-check"></i> Approve</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to mark this booking completed ?'))" class="dropdown-item"><i class="icon-check"></i> Completed</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to cancel this booking ?'))" class="dropdown-item"><i class="icon-reload-alt"></i> Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                    <span class="badge badge-success">Completed</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-eye"></i> View</a>
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to approve this booking ?'))" class="dropdown-item"><i class="icon-check"></i> Approve</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to mark this booking completed ?'))" class="dropdown-item"><i class="icon-check"></i> Completed</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to cancel this booking ?'))" class="dropdown-item"><i class="icon-reload-alt"></i> Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                    <span class="badge badge-success">Completed</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-eye"></i> View</a>
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to approve this booking ?'))" class="dropdown-item"><i class="icon-check"></i> Approve</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to mark this booking completed ?'))" class="dropdown-item"><i class="icon-check"></i> Completed</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to cancel this booking ?'))" class="dropdown-item"><i class="icon-reload-alt"></i> Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                    <span class="badge badge-success">Completed</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-eye"></i> View</a>
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to approve this booking ?'))" class="dropdown-item"><i class="icon-check"></i> Approve</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to mark this booking completed ?'))" class="dropdown-item"><i class="icon-check"></i> Completed</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to cancel this booking ?'))" class="dropdown-item"><i class="icon-reload-alt"></i> Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                </div>
                                
                        </div>
                        <div class="col-md-12 offset-6"></div>

                        <p> <strong>Note :</strong> data will be shown according Selected Date Above</p>
        </div>
        <div class="booking-content">
            <div class="new-booking-wrapper">
                <h4>All Booking list</h4>
                @include('flash')
            <div class="card">
                <form action="" class="form-wrapper">
                                    <div class="form-group">
                                        <input type="text" placeholder="Search by  Name , Booking ID , Email" class="form-control">
                                    </div>
                </form>
                <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Booking Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Jump Date</th>
                        <th>Status</th>
                        <th class="text-center" style="width: 30px;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($x=1)
                    @foreach($bookings as $booking)
                    <tr>
                        <td>#{{$booking->ref_id}}</td>
                        <td>{{$booking->first_name}} {{$booking->last_name}}</td>
                        <td><a href="#">{{$booking->email}}</a></td>
                        <td>{{$booking->jump_date}}</td>
                        <td>
                        @if ($booking->status == 'Completed')
                            <span class="badge badge-success">Completed</span>
                        @elseif($booking->status == 'Pending')
                            <span class="badge badge-warning">Pending</span>
                            @elseif($booking->status == 'Approved')
                            <span class="badge badge-primary">Approve</span>
                            @elseif($booking->status == 'Cancel')
                            <span class="badge badge-danger">Cancel</span>
                            @endif
                        </td>
                        <td class="text-center">
                            <div class="list-icons">
                                <div class="dropdown">
                                    <a href="#" class="list-icons-item" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="/admin/booking/{{$booking->id}}/view" class="dropdown-item"><i class="icon-eye"></i> View</a>
                                        <a href="/admin/booking/{{$booking->id}}/edit" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>
                                        <a href="/admin/booking/{{$booking->id}}/delete" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                        <a href="/admin/booking/{{$booking->ref_id}}/approve" onclick="return(confirm('Are you sure want to approve this booking ?'))" class="dropdown-item"><i class="icon-check"></i> Approve</a>
                                        <a href="/admin/booking/{{$booking->ref_id}}/completed" onclick="return(confirm('Are you sure want to mark this booking completed ?'))" class="dropdown-item"><i class="icon-check"></i> Completed</a>
                                        <a href="/admin/booking/{{$booking->ref_id}}/cancel" onclick="return(confirm('Are you sure want to cancel this booking ?'))" class="dropdown-item"><i class="icon-reload-alt"></i> Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            <div class="col-md-12 offset-6">{{ $bookings->links() }}</div>

            </div>
        </div>
           
        </div>
@endsection