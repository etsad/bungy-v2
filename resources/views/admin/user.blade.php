@extends('admin.layouts.layout')
@section('content')
  <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - User</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="booking-content">
                <div class="new-booking-wrapper">
                    <h4>  List of New users </h4>
                        <div class="card">
                                <form action="" class="form-wrapper">
                                    <div class="form-group">
                                        <input type="text" placeholder="Search by  Name , User ID , Email" class="form-control">
                                    </div>
                                </form>
                                 <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>User Id</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Joined Date</th>
                                                    <th>Records</th>
                                                    <th class="text-center" style="width: 30px;">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">0</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">0</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">01</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                                
                                            </tbody>
                                        </table>
                                </div>
                                
                        </div>
                        <div class="col-md-12 offset-6"></div>
                </div>
            </div>
        </div>


        <div class="content">
            <div class="booking-content">
                <div class="new-booking-wrapper">
                    <h4>  List of users </h4>
                        <div class="card">
                                <form action="" class="form-wrapper">
                                    <div class="form-group">
                                        <input type="text" placeholder="Search by  Name , User ID , Email" class="form-control">
                                    </div>
                                </form>
                                 <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>User Id</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Joined Date</th>
                                                    <th>Records</th>
                                                    <th class="text-center" style="width: 30px;">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">03</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/user-detail" class="dropdown-item"><i class="icon-pencil"></i> View Details</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                                
                                            </tbody>
                                        </table>
                                </div>
                                
                        </div>
                        <div class="col-md-12 offset-6"></div>
                </div>
            </div>
        </div>
    </div>


</div>



@endsection