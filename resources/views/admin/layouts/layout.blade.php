<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{env('SITE_NAME')}} - Admin Panel</title>


    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="/admin-assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/style.css" rel="stylesheet" type="text/css">
    <link href="/admin-assets/css/scss/main.css" rel="stylesheet" type="text/css">


    <script src="/admin-assets/js/main/jquery.min.js"></script>
    <script src="/admin-assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="/admin-assets/js/plugins/loaders/blockui.min.js"></script>


    <script src="/admin-assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script src="/admin-assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script src="/admin-assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script src="/admin-assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script src="/admin-assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="/admin-assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="/assets/js/pickadate/lib/picker.js"></script>
    <script src="/assets/js/pickadate/lib/picker.date.js"></script>
    <script src="/assets/js/pickadate/lib/picker.time.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>

    <script src="/admin-assets/js/app.js"></script>
    <script src="/admin-assets/js/demo_pages/dashboard.js"></script>
    <link rel="shortcut icon" type="image/png" href="{{ asset('vendor/laravel-filemanager/img/folder.png') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('vendor/laravel-filemanager/css/cropper.min.css') }}">
{{--    <style>{!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/css/lfm.css')) !!}</style>--}}
    <link rel="stylesheet" href="{{ asset('vendor/laravel-filemanager/css/mfb.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/laravel-filemanager/css/dropzone.min.css') }}">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">


</head>

<body>


<div class="navbar navbar-expand-md navbar-dark">
    <div class="navbar-brand">
        <a href="/admin/dashboard" class="d-inline-block">
            <img src="/assets/images/logo/logo.png" alt="">
        </a>
    </div>

    <div class="d-md-none">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-paragraph-justify3"></i>
                </a>
            </li>
        </ul>

        <span class="badge bg-success ml-md-3 mr-md-auto">Online</span>

        <ul class="navbar-nav">
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    <img src="/admin-assets/images/demo/users/face11.jpg" class="rounded-circle mr-2"
                         height="34" alt="">
                    <span>Administrator</span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    <a href="/admin/users/profile" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
                    <div class="dropdown-divider"></div>
                    <a href="/admin/logout" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
                </div>
            </li>
        </ul>
    </div>
</div>


<div class="page-content">
    <div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">


        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-main-toggle">
                <i class="icon-arrow-left8"></i>
            </a>
            Navigation
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
        </div>


        <div class="sidebar-content">


            <div class="sidebar-user">
                <div class="card-body">
                    <div class="media">
                        <div class="mr-3">
                            <a href="#"><img src="/admin-assets/images/demo/users/face11.jpg" width="38"
                                             height="38" class="rounded-circle" alt=""></a>
                        </div>

                        <div class="media-body">
                            <div class="media-title font-weight-semibold">{{\Illuminate\Support\Facades\Auth::user()->first_name}} {{\Illuminate\Support\Facades\Auth::user()->last_name}}</div>
                            <div class="font-size-xs opacity-50">
                                <i class="icon-pin font-size-sm"></i> {{env('SITE_ADDRESS')}}
                            </div>
                        </div>

                        <div class="ml-3 align-self-center">
                            <a href="#" class="text-white"><i class="icon-cog3"></i></a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="card card-sidebar-mobile">
                <ul class="nav nav-sidebar" data-nav-type="accordion">


                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">Main</div>
                        <i class="icon-menu" title="Main"></i></li>
                    <li class="nav-item">
                        <a href="/admin/dashboard" class="nav-link active">
                            <i class="icon-home4"></i>
                            <span>
									Dashboard
								</span>
                        </a>
                    </li>
                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">Booking Details</div>
                        <i class="icon-menu" title="Forms"></i></li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-copy"></i> <span>Booking Manager</span></a>

                        <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                            <li class="nav-item"><a href="/admin/booking-new" class="nav-link active">Make New Booking</a></li>
                            <li class="nav-item"><a href="/admin/booking" class="nav-link active">Manage Booking</a></li>
                        </ul>
                    </li>
                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">Content Management</div>
                        <i class="icon-menu" title="Forms"></i></li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-pencil3"></i> <span>Content Manager</span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="Form components">
                            <li class="nav-item"><a href="/admin/menu" class="nav-link">Menu Manager</a></li>
                            <li class="nav-item"><a href="/admin/page" class="nav-link">Page Manager</a></li>
                            <li class="nav-item"><a href="/admin" class="nav-link active">Work Manager</a></li>
                            <li class="nav-item"><a href="/admin" class="nav-link active">Testimonial Manager</a></li>
                            <li class="nav-item"><a href="/admin" class="nav-link active">Events Manager</a></li>
                        </ul>
                    </li>

                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">Media Details</div>
                        <i class="icon-menu" title="Forms"></i></li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-gallery"></i> <span>Media Manager</span></a>

                        <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                            <li class="nav-item"><a href="/admin/upload-media" class="nav-link active"> Upload Media</a></li>
                            <li class="nav-item"><a href="/admin/edit-media" class="nav-link active">Media Manager</a></li>
                            <li class="nav-item"><a href="/admin/banner-slider" class="nav-link active"> Banner Slider Manager</a></li>
                        </ul>
                    </li>

                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">Staff Details</div>
                        <i class="icon-menu" title="Forms"></i></li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-user"></i> <span>Staff Manager</span></a>

                        <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                            <li class="nav-item"><a href="/admin/add-staff" class="nav-link active">Add New Staff</a></li>
                            <li class="nav-item"><a href="/admin/staff-details" class="nav-link active">Staff Manager</a></li>
                        </ul>
                    </li>

                    <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">User Details</div>
                        <i class="icon-menu" title="Forms"></i></li>
                    <li class="nav-item nav-item-submenu">
                        <a href="#" class="nav-link"><i class="icon-user"></i> <span>Users Manager</span></a>

                        <ul class="nav nav-group-sub" data-submenu-title="Layouts">
                            <li class="nav-item"><a href="/admin/user" class="nav-link active">User Manager</a></li>
                        </ul>
                    </li>
                </ul>
            </div>


        </div>


    </div>

    @yield('content')

    {!! Menu::scripts() !!}

    <script>
        $(function () {
            $('.datepicker').pickadate({
                format: 'yyyy-mm-dd',
                min: true,
                max: false
            });
            $('#booking-form').validate();
        });

    </script>
</div>
</body>
</html>
