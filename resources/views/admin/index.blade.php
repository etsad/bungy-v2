@extends('admin.layouts.layout')
@section('content')
  <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - Dashboard</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>




        <div class="content">

            <div class="main-content">
                <div class="row">
                    <div class="col-md-4">
                        <div class="overview-card yellow">
                            <h4>New Bookings</h4>
                            <h1>13</h1>
                            <a href="">Go to bookings</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="overview-card blue">
                                <h4>New Users</h4>
                                <h1>3</h1>
                                <a href="">View Users</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="overview-card red">
                                <h4>Total Earnings </h4>
                                <h1>NRP: 1712</h1>
                                <a href="">Go to booking list</a>
                        </div>
                    </div>
                   
                </div>
            </div>

            <div class="booking-content">
                <div class="new-booking-wrapper">
                    <h4>  Today's Jumper List </h4>
                        <div class="card">
                                <form action="" class="form-wrapper">
                                    <div class="form-group">
                                        <input type="text" placeholder="Search by  Name , Booking ID , Email" class="form-control">
                                    </div>
                                </form>
                                 <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Booking Id</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Jump Date</th>
                                                    <th>Status</th>
                                                    <th class="text-center" style="width: 30px;">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">Approved</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-eye"></i> View</a>
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to approve this booking ?'))" class="dropdown-item"><i class="icon-check"></i> Approve</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to mark this booking completed ?'))" class="dropdown-item"><i class="icon-check"></i> Completed</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to cancel this booking ?'))" class="dropdown-item"><i class="icon-reload-alt"></i> Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">Approved</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-eye"></i> View</a>
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to approve this booking ?'))" class="dropdown-item"><i class="icon-check"></i> Approve</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to mark this booking completed ?'))" class="dropdown-item"><i class="icon-check"></i> Completed</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to cancel this booking ?'))" class="dropdown-item"><i class="icon-reload-alt"></i> Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">Approved</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-eye"></i> View</a>
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to approve this booking ?'))" class="dropdown-item"><i class="icon-check"></i> Approve</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to mark this booking completed ?'))" class="dropdown-item"><i class="icon-check"></i> Completed</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to cancel this booking ?'))" class="dropdown-item"><i class="icon-reload-alt"></i> Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">Approved</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-eye"></i> View</a>
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to approve this booking ?'))" class="dropdown-item"><i class="icon-check"></i> Approve</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to mark this booking completed ?'))" class="dropdown-item"><i class="icon-check"></i> Completed</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to cancel this booking ?'))" class="dropdown-item"><i class="icon-reload-alt"></i> Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                </div>
                                
                        </div>
                        <div class="col-md-12 offset-6"></div>
                </div>
            </div>

            <div class="booking-content">
                <div class="new-booking-wrapper">
                    <h4>  New Booking Notifications </h4>
                        <div class="card">
                                <form action="" class="form-wrapper">
                                    <div class="form-group">
                                        <input type="text" placeholder="Search by  Name , Booking ID , Email" class="form-control">
                                    </div>
                                </form>
                                 <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Booking Id</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Jump Date</th>
                                                    <th>Status</th>
                                                    <th class="text-center" style="width: 30px;">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-warning">Pending</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-eye"></i> View</a>
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to approve this booking ?'))" class="dropdown-item"><i class="icon-check"></i> Approve</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to mark this booking completed ?'))" class="dropdown-item"><i class="icon-check"></i> Completed</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to cancel this booking ?'))" class="dropdown-item"><i class="icon-reload-alt"></i> Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-warning">Pending</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-eye"></i> View</a>
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to approve this booking ?'))" class="dropdown-item"><i class="icon-check"></i> Approve</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to mark this booking completed ?'))" class="dropdown-item"><i class="icon-check"></i> Completed</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to cancel this booking ?'))" class="dropdown-item"><i class="icon-reload-alt"></i> Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-warning">Pending</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-eye"></i> View</a>
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to approve this booking ?'))" class="dropdown-item"><i class="icon-check"></i> Approve</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to mark this booking completed ?'))" class="dropdown-item"><i class="icon-check"></i> Completed</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to cancel this booking ?'))" class="dropdown-item"><i class="icon-reload-alt"></i> Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-warning">Pending</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-eye"></i> View</a>
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to approve this booking ?'))" class="dropdown-item"><i class="icon-check"></i> Approve</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to mark this booking completed ?'))" class="dropdown-item"><i class="icon-check"></i> Completed</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to cancel this booking ?'))" class="dropdown-item"><i class="icon-reload-alt"></i> Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-warning">Pending</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-eye"></i> View</a>
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to approve this booking ?'))" class="dropdown-item"><i class="icon-check"></i> Approve</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to mark this booking completed ?'))" class="dropdown-item"><i class="icon-check"></i> Completed</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to cancel this booking ?'))" class="dropdown-item"><i class="icon-reload-alt"></i> Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-warning">Pending</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-eye"></i> View</a>
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to approve this booking ?'))" class="dropdown-item"><i class="icon-check"></i> Approve</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to mark this booking completed ?'))" class="dropdown-item"><i class="icon-check"></i> Completed</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to cancel this booking ?'))" class="dropdown-item"><i class="icon-reload-alt"></i> Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a href="#">james@mail.com</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-warning">Pending</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <div class="list-icons">
                                                            <div class="dropdown">
                                                                <a href="#" class="list-icons-item" data-toggle="dropdown">
                                                                    <i class="icon-menu9"></i>
                                                                </a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-eye"></i> View</a>
                                                                    <a href="/admin/booking/" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to delete this booking ?'))" class="dropdown-item"><i class="icon-trash"></i> Delete</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to approve this booking ?'))" class="dropdown-item"><i class="icon-check"></i> Approve</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to mark this booking completed ?'))" class="dropdown-item"><i class="icon-check"></i> Completed</a>
                                                                    <a href="/admin/booking/" onclick="return(confirm('Are you sure want to cancel this booking ?'))" class="dropdown-item"><i class="icon-reload-alt"></i> Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                </div>
                                <div class="ancor-division">
                                    <a href=""><span class="pull-right icon-arrow-right16"></span> View all bookings </a>
                                </div>
                        </div>
                        <div class="col-md-12 offset-6"></div>
                </div>
            </div>

            

        </div>
    </div>


</div>



@endsection