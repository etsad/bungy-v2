@extends('admin.layouts.layout')
@section('content')
  <div class="content-wrapper">
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Home</span> - User</h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>




        <div class="content">
            <div class="booking-content">
                <div class="new-booking-wrapper">
                    <h4>  Records Details of James</h4>
                    <div class="export-content">
                    <form action="" class="form-wrapper">
                <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <button class="btn btn-success">Export to Excel</button>
                    </div>   
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <button class="btn btn-danger">Export to PDF</button>
                    </div>   
                </div>
                </div>
            </form>
            <div class="card">
                            <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Booking Id</th>
                                                    <th>Jumper Name</th>
                                                    <th>Jumper Citizen</th>
                                                    <th>Jump Date</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>James Harden</td>
                                                    <td><a>660</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-info">approved</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>Nikki Harden</td>
                                                    <td><a>660</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-success">Completed</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>#45632</td>
                                                    <td>Nikki Harden</td>
                                                    <td><a>660</a></td>
                                                    <td>2018-08-09</td>
                                                    <td> 
                                                        <span class="badge badge-danger">Completed</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                </div>
                        </div>
                    </div>
                        
                        <div class="col-md-12 offset-6"></div>
                </div>
            </div>
        </div>
    </div>


</div>



@endsection