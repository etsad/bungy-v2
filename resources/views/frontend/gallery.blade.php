@extends('frontend.layouts.layout')
@section('content')
    <section class="gallery-wrapper">
        <div class="gallery-banner">
            <div class="container">
                <div class="heading-text">
                    <h1>Gallery</h1>
                    <h4>Take a look what you can feel</h4>
                </div>
            </div>

        </div>
    </section>
    <section class="gallery-body">
        <div class="container">
            <div class="gallery-body-wrapper">
                <div class="row">
                    <div class="col-md-2">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#photos" data-toggle="tab">Photos</a></li>
                            <li><a href="#video" data-toggle="tab">Videos</a></li>
                        </ul>
                    </div>
                    <div class="col-md-10">
                        <div class="tab-content">
                            <div class="tab-pane active" id="photos">
                                <div class="row">
                                    <div class="col-md-4">
                                        <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt=""/></a>
                                    </div>
                                    <div class="col-md-4">
                                        <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt=""/></a>
                                    </div>
                                    <div class="col-md-4">
                                        <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt=""/></a>
                                    </div>
                                    <div class="col-md-4">
                                        <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt=""/></a>
                                    </div>
                                    <div class="col-md-4">
                                        <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt=""/></a>
                                    </div>
                                    <div class="col-md-4">
                                        <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt=""/></a>
                                    </div>
                                    <div class="col-md-4">
                                        <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt=""/></a>
                                    </div>
                                    <div class="col-md-4">
                                        <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt=""/></a>
                                    </div>
                                    <div class="col-md-4">
                                        <a class="example-image-link" href="http://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image" src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt=""/></a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="video">
                                <div class="col-md-12">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
                                    </div>
                                    <div class="heading-text">
                                        <h4>Title of image</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium assumenda at commodi consequuntur debitis dolorem doloribus error, illo in itaque iure mollitia nobis qui quo quos rem sapiente sequi voluptatum.</p>
                                    </div>
                                    <br>
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
                                    </div>
                                    <div class="heading-text">
                                        <h4>Title of image</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium assumenda at commodi consequuntur debitis dolorem doloribus error, illo in itaque iure mollitia nobis qui quo quos rem sapiente sequi voluptatum.</p>
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
