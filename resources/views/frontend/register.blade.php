@extends('frontend.layouts.layout')
@section('content')
<section class="account-wrapper">
<div class="container">
    <div class="account-body-wrapper">
        <div class="row">
            <div class="col-md-6">
<!--                <div class="heading-text">-->
<!--                    <h1>Login</h1>-->
<!--                    <h1>To Your Account</h1>-->
<!--                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A alias asperiores blanditiis commodi cum earum ex excepturi, facilis iure nemo neque nihil odio praesentium quaerat ullam vel veniam voluptatem voluptatum?</p>-->
<!--                </div>-->
            </div>
            <div class="col-md-6">
                <div class="account-body">
                    <div class="account-form-wrapper">
                        <div class="heading-text">
                            <div class="h4">
                                <h4>Create New Account</h4>
                                <p>I already have my account <a href="/login">Login Now</a></p>
                            </div>
                            <form action="/register" method="post" id="register-form">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">First Name</label>
                                            <input type="text" name="first_name" required class="form-control" >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="">Last Name</label>
                                            <input type="text" name="last_name" required class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Enter Email Address</label>
                                    <input type="email" name="email" required class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Your Password</label>
                                    <input type="password" name="password" required class="form-control">
                                </div>
                                <div class="form-group buttons">
                                    <button class="btn btn-black">Register Now</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

@endsection
