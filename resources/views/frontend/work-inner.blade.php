@extends('frontend.layouts.layout')
@section('content')
<section class="inner-banner works">

</section>
<section class="work-story">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <a class="example-image-link" href="https://via.placeholder.com/1000x550" data-lightbox="example-set" data-title=""><img class="example-image" src="https://via.placeholder.com/950x350" alt=""/></a>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-3">
            <a class="example-image-link" href="https://via.placeholder.com/1000x550" data-lightbox="example-set" data-title=""><img class="example-image" src="https://via.placeholder.com/220x200" alt=""/></a>
        </div>
        <div class="col-md-3">
            <a class="example-image-link" href="https://via.placeholder.com/1000x550" data-lightbox="example-set" data-title=""><img class="example-image" src="https://via.placeholder.com/220x200" alt=""/></a>
        </div>
        <div class="col-md-3">
            <a class="example-image-link" href="https://via.placeholder.com/1000x550" data-lightbox="example-set" data-title=""><img class="example-image" src="https://via.placeholder.com/220x200" alt=""/></a>
        </div>
        <div class="col-md-3">
            <a class="example-image-link" href="https://via.placeholder.com/1000x550" data-lightbox="example-set" data-title=""><img class="example-image" src="https://via.placeholder.com/220x200" alt=""/></a>
        </div>
    </div>
    <br>
    <div class="work-text">
        <div class="heading-text">
            <h4>Work Titile Will Be Written Here</h4>
        </div>
        <div class="description">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias at corporis culpa, distinctio doloribus facilis fuga fugit ipsum magni natus neque quibusdam sed soluta suscipit tempora voluptate, voluptatem, voluptates? Ex? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam asperiores commodi, dolores ducimus earum fuga harum itaque libero molestias natus nesciunt, non numquam odit quae ratione saepe sed temporibus velit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci alias architecto asperiores doloribus ducimus earum excepturi exercitationem, fugit impedit inventore laboriosam laudantium praesentium quam quasi quibusdam sapiente sed sit ut.</p>
        </div>
    </div>
    <br>
    <div class="buttons">
        <a href="works.blade.php" class="btn btn-outline-black" ><span class="ion-android-arrow-back"></span> Back To Works</a>
    </div>

</div>
</section>
@endsection
