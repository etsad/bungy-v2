@extends('frontend.layouts.layout')
@section('content')
<section class="account-wrapper">
<div class="container">
    <div class="account-body-wrapper">
        <div class="row">
            <div class="col-md-6">

            </div>
            <div class="col-md-6">
                <div class="account-body">
                    @include('flash')

                    <div class="account-form-wrapper">
                        <div class="heading-text">
                            <div class="h4">
                                <h4>Login To Account</h4>
                                <p>I don't have my account yet <a href="/register">Register New</a></p>
                            </div>
                            <form action="/authenticate" method="post" id="login-form">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="">Email Address</label>
                                    <input type="email" name="email" required  class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label for="">Your Password</label>
                                    <input type="password" name="password" class="form-control">
                                    <p>Oops I seems I <a href="">forgot</a> my password </p>
                                </div>
                                <div class="form-group buttons">
                                    <button class="btn btn-black">Login Now</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

@endsection
