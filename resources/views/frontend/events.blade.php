@extends('frontend.layouts.layout')
@section('content')
<section class="gallery-wrapper">
    <div class="gallery-banner">
        <div class="container">
            <div class="heading-text">
                <h1>Upcoming events</h1>
                <h4>Take a look what you can feel</h4>
            </div>
        </div>

    </div>
</section>
<section class="gallery-body">
        <div class="container">
            <div class="gallery-body-wrapper">
                <div class="row">
                    <div class="col-md-4">
                        <ul class="nav nav-tabs list-unstyled">
                            <li class="active">
                                <a href="#tab1" data-toggle="tab">1. Event title goes here</a>
                            </li>
                            <li><a href="#tab2" data-toggle="tab">2. Next Event title goes here</a></li>
                            <li><a href="#tab3" data-toggle="tab">3. Next Event title goes here</a></li>
                            <li><a href="#tab4" data-toggle="tab">4. Next Event title goes here</a></li>
                        </ul>
                    </div>
                    <div class="col-md-8">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1">
                                <div class="event-wrapper">

                                    <a class="example-image-link" href="https://via.placeholder.com/1000x550" data-lightbox="example-set" data-title=""><img class="example-image img-responsive" src="https://via.placeholder.com/670x240" alt=""/></a>
                                    <br>
                                    <div class="descrption">
                                        <p>Date: 12th December 2018</p>
                                    </div>
                                    <div class="heading-text">
                                        <h4>Events Heading tagline goes Here</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda doloremque dolores ex, illo laborum laudantium mollitia nobis qui ratione rem rerum, vero. Ab id incidunt nobis tempora ut, voluptas voluptate.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aspernatur atque corporis dicta doloribus esse eum, eveniet, facilis fugit id iste itaque laboriosam nam quam quia repudiandae vero voluptatibus, voluptatum.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aspernatur atque corporis dicta doloribus esse eum, eveniet, facilis fugit id iste itaque laboriosam nam quam quia repudiandae vero voluptatibus, voluptatum.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aspernatur atque corporis dicta doloribus esse eum, eveniet, facilis fugit id iste itaque laboriosam nam quam quia repudiandae vero voluptatibus, voluptatum.</p>

                                    </div>
                                    <hr>
                                    <div class="heading-text">
                                        <h4>Share This Events</h4>
                                    </div>
                                    <div class="buttons">
                                        <br>
                                        <ul class="list-inline">
                                            <li><a href="" class="btn-outline-black"> FACEBOOK</a></li>
                                            <li><a href="" class="btn-outline-black"> TWITTER</a></li>
                                            <li><a href="" class="btn-outline-black"> GOOGLE PLUS</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab2">
                                <div class="event-wrapper">

                                    <a class="example-image-link" href="https://via.placeholder.com/1000x550" data-lightbox="example-set" data-title=""><img class="example-image img-responsive" src="https://via.placeholder.com/670x240" alt=""/></a>
                                    <br>
                                    <div class="descrption">
                                        <p>Date: 12th December 2018</p>
                                    </div>
                                    <div class="heading-text">
                                        <h4>Events Heading tagline goes Here</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda doloremque dolores ex, illo laborum laudantium mollitia nobis qui ratione rem rerum, vero. Ab id incidunt nobis tempora ut, voluptas voluptate.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aspernatur atque corporis dicta doloribus esse eum, eveniet, facilis fugit id iste itaque laboriosam nam quam quia repudiandae vero voluptatibus, voluptatum.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aspernatur atque corporis dicta doloribus esse eum, eveniet, facilis fugit id iste itaque laboriosam nam quam quia repudiandae vero voluptatibus, voluptatum.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aspernatur atque corporis dicta doloribus esse eum, eveniet, facilis fugit id iste itaque laboriosam nam quam quia repudiandae vero voluptatibus, voluptatum.</p>

                                    </div>
                                    <hr>
                                    <div class="heading-text">
                                        <h4>Share This Events</h4>
                                    </div>
                                    <div class="buttons">
                                        <br>
                                        <ul class="list-inline">
                                            <li><a href="" class="btn-outline-black"> FACEBOOK</a></li>
                                            <li><a href="" class="btn-outline-black"> TWITTER</a></li>
                                            <li><a href="" class="btn-outline-black"> GOOGLE PLUS</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab3">
                                <div class="event-wrapper">

                                    <a class="example-image-link" href="https://via.placeholder.com/1000x550" data-lightbox="example-set" data-title=""><img class="example-image img-responsive" src="https://via.placeholder.com/670x240" alt=""/></a>
                                    <br>
                                    <div class="descrption">
                                        <p>Date: 12th December 2018</p>
                                    </div>
                                    <div class="heading-text">
                                        <h4>Events Heading tagline goes Here</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda doloremque dolores ex, illo laborum laudantium mollitia nobis qui ratione rem rerum, vero. Ab id incidunt nobis tempora ut, voluptas voluptate.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aspernatur atque corporis dicta doloribus esse eum, eveniet, facilis fugit id iste itaque laboriosam nam quam quia repudiandae vero voluptatibus, voluptatum.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aspernatur atque corporis dicta doloribus esse eum, eveniet, facilis fugit id iste itaque laboriosam nam quam quia repudiandae vero voluptatibus, voluptatum.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aspernatur atque corporis dicta doloribus esse eum, eveniet, facilis fugit id iste itaque laboriosam nam quam quia repudiandae vero voluptatibus, voluptatum.</p>

                                    </div>
                                    <hr>
                                    <div class="heading-text">
                                        <h4>Share This Events</h4>
                                    </div>
                                    <div class="buttons">
                                        <br>
                                        <ul class="list-inline">
                                            <li><a href="" class="btn-outline-black"> FACEBOOK</a></li>
                                            <li><a href="" class="btn-outline-black"> TWITTER</a></li>
                                            <li><a href="" class="btn-outline-black"> GOOGLE PLUS</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab4">
                                <div class="event-wrapper">

                                    <a class="example-image-link" href="https://via.placeholder.com/1000x550" data-lightbox="example-set" data-title=""><img class="example-image img-responsive" src="https://via.placeholder.com/670x240" alt=""/></a>
                                    <br>
                                    <div class="descrption">
                                        <p>Date: 12th December 2018</p>
                                    </div>
                                    <div class="heading-text">
                                        <h4>Events Heading tagline goes Here</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda doloremque dolores ex, illo laborum laudantium mollitia nobis qui ratione rem rerum, vero. Ab id incidunt nobis tempora ut, voluptas voluptate.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aspernatur atque corporis dicta doloribus esse eum, eveniet, facilis fugit id iste itaque laboriosam nam quam quia repudiandae vero voluptatibus, voluptatum.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aspernatur atque corporis dicta doloribus esse eum, eveniet, facilis fugit id iste itaque laboriosam nam quam quia repudiandae vero voluptatibus, voluptatum.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aspernatur atque corporis dicta doloribus esse eum, eveniet, facilis fugit id iste itaque laboriosam nam quam quia repudiandae vero voluptatibus, voluptatum.</p>

                                    </div>
                                    <hr>
                                    <div class="heading-text">
                                        <h4>Share This Events</h4>
                                    </div>
                                    <div class="buttons">
                                        <br>
                                        <ul class="list-inline">
                                            <li><a href="" class="btn-outline-black"> FACEBOOK</a></li>
                                            <li><a href="" class="btn-outline-black"> TWITTER</a></li>
                                            <li><a href="" class="btn-outline-black"> GOOGLE PLUS</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection
