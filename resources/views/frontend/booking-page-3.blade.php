@extends('frontend.layouts.layout')
@section('content')
<section class="inner-booking">
        <div class="container">

            <div class="selection-form-group">
                <div class="heading-text">
                    <h1>Booking ID #{{$bookingData[0]->ref_id}}</h1>
                    <h4>Your Jump Has Been Successfully Booked</h4>
                </div>
                <div class="description">
                    <p style="text-align: left">Dear {{$bookingData[0]->first_name}} {{$bookingData[0]->last_name}}, Your booking is in the progress you will be shortly notified with your email({{$data['email']}}).</p>
                    <p style="text-align: left">If Something went wrong please try to contact our office at number {{env('SITE_NUMBER')}} or via {{env('SITE_EMAIL')}}</p>

                </div>
                @if(\Illuminate\Support\Facades\Auth::user())
                <div class="buttons">
                    <a href="/account" class="btn btn-black"> View My Account</a>
                </div>
                @endif
            </div>
        </div>
</section>
@endsection
