@extends('frontend.layouts.layout')
@section('content')
<section class="inner-banner works">
    <div class="container">
        <div class="terms">
            <div class="heading-text">
                <h4>Terms & Conditions</h4>
            </div>
        </div>
    </div>
</section>
<section class="terms-wrapper">
    <div class="container">
        <div class="terms-box">
            <div class="description">
                <p><strong>Title of heading </strong></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eligendi eveniet iure officia placeat quasi ut, velit voluptate. Assumenda facere inventore perspiciatis! Assumenda deserunt id molestias perspiciatis repudiandae sequi velit!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad assumenda at blanditiis, deleniti error est et fugit ipsum, minima mollitia non officia praesentium quisquam quos repudiandae sint sunt tempora voluptatibus!</p>
            </div>
            <ul class="list-unstyled">
                <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aut autem consectetur cum cumque dignissimos eos error, explicabo inventore iure iusto natus nisi, non sapiente, sequi sunt suscipit totam voluptates.</li>
                <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aut autem consectetur cum cumque dignissimos eos error, explicabo inventore iure iusto natus nisi, non sapiente, sequi sunt suscipit totam voluptates.</li>
                <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aut autem consectetur cum cumque dignissimos eos error, explicabo inventore iure iusto natus nisi, non sapiente, sequi sunt suscipit totam voluptates.</li>
                <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aut autem consectetur cum cumque dignissimos eos error, explicabo inventore iure iusto natus nisi, non sapiente, sequi sunt suscipit totam voluptates.</li>
                <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aut autem consectetur cum cumque dignissimos eos error, explicabo inventore iure iusto natus nisi, non sapiente, sequi sunt suscipit totam voluptates.</li>
                <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aut autem consectetur cum cumque dignissimos eos error, explicabo inventore iure iusto natus nisi, non sapiente, sequi sunt suscipit totam voluptates.</li>
                <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aut autem consectetur cum cumque dignissimos eos error, explicabo inventore iure iusto natus nisi, non sapiente, sequi sunt suscipit totam voluptates.</li>
            </ul>
        </div>
        <div class="terms-box">
            <div class="description">
                <p><strong>Title of heading </strong></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eligendi eveniet iure officia placeat quasi ut, velit voluptate. Assumenda facere inventore perspiciatis! Assumenda deserunt id molestias perspiciatis repudiandae sequi velit!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad assumenda at blanditiis, deleniti error est et fugit ipsum, minima mollitia non officia praesentium quisquam quos repudiandae sint sunt tempora voluptatibus!</p>
            </div>

        </div>
        <div class="terms-box">
            <div class="description">
                <p><strong>Title of heading </strong></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eligendi eveniet iure officia placeat quasi ut, velit voluptate. Assumenda facere inventore perspiciatis! Assumenda deserunt id molestias perspiciatis repudiandae sequi velit!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad assumenda at blanditiis, deleniti error est et fugit ipsum, minima mollitia non officia praesentium quisquam quos repudiandae sint sunt tempora voluptatibus!</p>
            </div>
            <ul class="list-unstyled">
                <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aut autem consectetur cum cumque dignissimos eos error, explicabo inventore iure iusto natus nisi, non sapiente, sequi sunt suscipit totam voluptates.</li>
                <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aut autem consectetur cum cumque dignissimos eos error, explicabo inventore iure iusto natus nisi, non sapiente, sequi sunt suscipit totam voluptates.</li>
                <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aut autem consectetur cum cumque dignissimos eos error, explicabo inventore iure iusto natus nisi, non sapiente, sequi sunt suscipit totam voluptates.</li>
                <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aut autem consectetur cum cumque dignissimos eos error, explicabo inventore iure iusto natus nisi, non sapiente, sequi sunt suscipit totam voluptates.</li>
                <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aut autem consectetur cum cumque dignissimos eos error, explicabo inventore iure iusto natus nisi, non sapiente, sequi sunt suscipit totam voluptates.</li>
                <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aut autem consectetur cum cumque dignissimos eos error, explicabo inventore iure iusto natus nisi, non sapiente, sequi sunt suscipit totam voluptates.</li>
                <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto aut autem consectetur cum cumque dignissimos eos error, explicabo inventore iure iusto natus nisi, non sapiente, sequi sunt suscipit totam voluptates.</li>
            </ul>
        </div>
        <div class="terms-box">
            <div class="description">
                <p><strong>Title of heading </strong></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eligendi eveniet iure officia placeat quasi ut, velit voluptate. Assumenda facere inventore perspiciatis! Assumenda deserunt id molestias perspiciatis repudiandae sequi velit!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad assumenda at blanditiis, deleniti error est et fugit ipsum, minima mollitia non officia praesentium quisquam quos repudiandae sint sunt tempora voluptatibus!</p>
            </div>

        </div>
        <div class="terms-box">
            <div class="description">
                <p><strong>Title of heading </strong></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eligendi eveniet iure officia placeat quasi ut, velit voluptate. Assumenda facere inventore perspiciatis! Assumenda deserunt id molestias perspiciatis repudiandae sequi velit!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad assumenda at blanditiis, deleniti error est et fugit ipsum, minima mollitia non officia praesentium quisquam quos repudiandae sint sunt tempora voluptatibus!</p>
            </div>

        </div>
        <div class="terms-box">
            <div class="description">
                <p><strong>Title of heading </strong></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eligendi eveniet iure officia placeat quasi ut, velit voluptate. Assumenda facere inventore perspiciatis! Assumenda deserunt id molestias perspiciatis repudiandae sequi velit!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad assumenda at blanditiis, deleniti error est et fugit ipsum, minima mollitia non officia praesentium quisquam quos repudiandae sint sunt tempora voluptatibus!</p>
            </div>

        </div>
    </div>
</section>
@endsection
