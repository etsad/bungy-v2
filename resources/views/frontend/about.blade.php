@extends('frontend.layouts.layout')
@section('content')
    <section class="inner-banner">
        <div class="banner">
            <img src="assets/images/banner-bungy.jpg" alt="bungy-banner1" class="img-responsive">
        </div>
    </section>

    <section class="inner-con-text">
    <div class="container">
    <div class="heading-text text-center">
    <h4>About Us</h4>
    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Assumenda vero possimus nostrum blanditiis aspernatur quasi porro earum animi cum, similique ullam tempore saepe, ea ex maiores dicta ipsam, aut doloribus. Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus consequatur in minus corrupti, mollitia sunt accusamus eaque molestias rem odit eius corporis repudiandae, tenetur odio debitis officia maiores error! Hic! Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aliquam maxime sapiente quo, provident at obcaecati debitis enim officiis explicabo eaque cumque, quibusdam expedita esse numquam soluta dolorem. Sint, sed veritatis.</p>
    </div>
    <br>
    <div class="about-img">
    <div class="row">
    <div class="col-md-6">
    <img src="assets/images/banner-bungy.jpg" alt="bungy-banner1" class="img-responsive">
    <div class="">
        <h4>Title of image about</h4>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora, distinctio. Animi, ex accusamus quod blanditiis tenetur eligendi libero impedit modi atque fugiat, laudantium ducimus est asperiores cumque distinctio saepe doloremque.</p>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit debitis omnis voluptate vel reprehenderit. Sunt quasi laborum aliquam, alias nisi vero quibusdam eligendi tenetur nesciunt quisquam earum odio eaque mollitia.</p>
    </div>
    </div>
    <div class="col-md-6">
    <img src="assets/images/banner-bungy.jpg" alt="bungy-banner1" class="img-responsive">
    <div class="">
        <h4>Title of image about</h4>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora, distinctio. Animi, ex accusamus quod blanditiis tenetur eligendi libero impedit modi atque fugiat, laudantium ducimus est asperiores cumque distinctio saepe doloremque.</p>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem et, velit aliquam labore neque perferendis doloribus aspernatur totam quisquam fugiat corporis id dolore, deleniti deserunt. Consequatur a velit et officia?</p>
    </div>
    </div>
    </div>
    </div>
    </div>
    </section>
    <section class="bungy-works">
    <div class="container">
        <div class="heading-text">
            <h3>See Our</h3>
            <h4>Latest Events</h4>
        </div>
        <div class="work-slider">
            <div class="work-box">
                <div class="row">
                    <div class="col-md-4">
                        <figure>
                            <img src="assets/images/bungee-jump.jpg" alt="bungy-nepal" class="img-responsive">
                        </figure>
                    </div>
                    <div class="col-md-8">
                        <ul class="list-unstyled">
                            <li class="title">A Jump by Mr Bhattrai</li>
                            <li class="description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet,
                                consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Laudantium quo veritatis vero. Architecto at doloribus ducimus earum et fugiat ipsam
                                laudantium modi molestias nobis perferendis possimus quasi, ratione temporibus voluptas?
                            </li>
                            <li class="bunjee-cat">
                                <span>Santosh Bhattrai</span>
                                <br>
                                <i>published on : 11th June 2018</i>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="work-box">
                <div class="row">
                    <div class="col-md-4">
                        <figure>
                            <img src="assets/images/bungee-jump.jpg" alt="bungy-nepal" class="img-responsive">
                        </figure>
                    </div>
                    <div class="col-md-8">
                        <ul class="list-unstyled">
                            <li class="title">A Jump by Mr Bhattrai</li>
                            <li class="description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet,
                                consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Laudantium quo veritatis vero. Architecto at doloribus ducimus earum et fugiat ipsam
                                laudantium modi molestias nobis perferendis possimus quasi, ratione temporibus voluptas?
                            </li>
                            <li class="bunjee-cat">
                                <span>Santosh Bhattrai</span>
                                <br>
                                <i>published on : 11th June 2018</i>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="work-box">
                <div class="row">
                    <div class="col-md-4">
                        <figure>
                            <img src="assets/images/bungee-jump.jpg" alt="bungy-nepal" class="img-responsive">
                        </figure>
                    </div>
                    <div class="col-md-8">
                        <ul class="list-unstyled">
                            <li class="title">A Jump by Mr Bhattrai</li>
                            <li class="description">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet,
                                consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Laudantium quo veritatis vero. Architecto at doloribus ducimus earum et fugiat ipsam
                                laudantium modi molestias nobis perferendis possimus quasi, ratione temporibus voluptas?
                            </li>
                            <li class="bunjee-cat">
                                <span>Santosh Bhattrai</span>
                                <br>
                                <i>published on : 11th June 2018</i>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    
    
@endsection
