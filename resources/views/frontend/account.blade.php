@extends('frontend.layouts.layout')
@section('content')
<section class="account-wrapper">
<div class="container">
    <div class="account-body-wrapper">
        <div class="my-account-wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-text">
                        <h4>Welcome back {{\Illuminate\Support\Facades\Auth::user()->first_name}} {{\Illuminate\Support\Facades\Auth::user()->last_name}}</h4>
                    </div>
                    <div>@include('flash')</div>
                    <div class="tab-container">
                        <ul class="nav nav-tabs nav-tabs-left nav-centered" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#bookings" data-toggle="tab" role="tab">
                                    My Bookings
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#account" data-toggle="tab" role="tab">
                                    My Account
                                </a>
                            </li>
                        </ul>
                        <div id="my_side_tabs" class="tab-content side-tabs side-tabs-left">
                            <div class="tab-pane fade in active" id="bookings" role="tabpanel">
                               <div class="heading-text">
                                   <h4>Active Booking <span>| {{count($activeBooking)}} </span></h4>
                               </div>
                                <div class="row">
                                    @foreach($activeBooking as $bookingDetail)
                                    <div class="col-md-4">
                                        <div class="booking-section">
                                            <p>
                                                <strong><span class="denote">Booking ID:</span> #{{$bookingDetail->ref_id}}</strong>
                                            </p>
                                            <p>
                                                <strong><span class="denote">Jump Date:</span> {{$bookingDetail->jump_date}}</strong>
                                            </p>
                                            <p>
                                                <strong><span class="denote">Type:</span> {{$bookingDetail->user_type}}</strong>
                                            </p>
                                            <p>
                                                <strong><span class="denote">C/P ID:</span> {{$bookingDetail->citizenship_number}}</strong>
                                            </p>
                                            <p>
                                                <strong><span class="denote">Full Name:</span>{{$bookingDetail->first_name}}</strong>
                                            </p>
                                            <p>
                                                <strong><span class="denote">Gender:</span> {{$bookingDetail->gender}}</strong>
                                            </p>
                                            <p>
                                                <strong><span class="denote">Age:</span> {{$bookingDetail->age}}</strong>
                                            </p>
                                            <hr>
                                            <div class="buttons">
                                                <a onclick="return confirm('Are you sure want to cancel this booking?')" href="/bookings/{{$bookingDetail->ref_id}}/cancel" class="btn btn-black full-width">Cancel This Booking</a>
                                            </div>

                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <hr>
                                <div class="heading-text">
                                    <h4>Completed Bookings <span>| {{count($completedBooking)}}</span></h4>
                                </div>
                                <div class="row">
                                    @foreach($completedBooking as $CompletedBookingDetail)
                                        <div class="col-md-4">
                                            <div class="booking-section">
                                                <p>
                                                    <strong><span class="denote">Booking ID:</span> #{{$CompletedBookingDetail->ref_id}}</strong>
                                                </p>
                                                <p>
                                                    <strong><span class="denote">Jump Date:</span> {{$CompletedBookingDetail->jump_date}}</strong>
                                                </p>
                                                <p>
                                                    <strong><span class="denote">Type:</span> {{$CompletedBookingDetail->user_type}}</strong>
                                                </p>
                                                <p>
                                                    <strong><span class="denote">C/P ID:</span> {{$CompletedBookingDetail->citizenship_number}}</strong>
                                                </p>
                                                <p>
                                                    <strong><span class="denote">Full Name:</span>{{$CompletedBookingDetail->first_name}}</strong>
                                                </p>
                                                <p>
                                                    <strong><span class="denote">Gender:</span> {{$CompletedBookingDetail->gender}}</strong>
                                                </p>
                                                <p>
                                                    <strong><span class="denote">Age:</span> {{$CompletedBookingDetail->age}}</strong>
                                                </p>
                                                <hr>
                                            </div>
                                        </div>
                                    @endforeach
                            </div>
                        </div>
                            <div class="tab-pane fade" id="account" role="tabpanel">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="heading-text">
                                            <h4>Change Your Password</h4>
                                            <p>Fill up below form inorder to change password</p>
                                        </div>
                                        <hr>
                                        <form action="/change-password" method="post">
                                            {{csrf_field()}}
                                                <div class="form-group">
                                                    <label for="">Enter Current Password</label>
                                                    <input type="password" required name="current_password" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Enter New Password</label>
                                                    <input type="password" required name="new_password" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Verify New Password</label>
                                                    <input type="password" name="confirm_password" class="form-control">
                                                </div>
                                            <div class="form-group buttons">
                                                <button class="btn btn-black full-width">Change & Update Password</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="heading-text">
                                            <h4>Delete My Account</h4>
                                            <p>It will delete your overall data & bookings you have made</p>
                                        </div>
                                        <hr>
                                        <form action="/delete-account" method="post">
                                            {{csrf_field()}}
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="user_id" required value="{{\Illuminate\Support\Facades\Auth::id()}}">Delete my all data related to this account</label>
                                            </div>
                                            <div class="buttons">
                                                <button class="btn btn-black full-width">Delete My Account</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>

@endsection
