@extends('frontend.layouts.layout')
@section('content')
<section class="inner-banner works">
    <div class="container">
        <div class="terms">
            <div class="heading-text">
                <h4>Careers</h4>

            </div>
        </div>
    </div>
</section>
<section class="terms-wrapper">
    <div class="container">
        <div class="terms-box">
            <div class="description">
                <h1><strong>Title of Job </strong></h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum eligendi eveniet iure officia placeat quasi ut, velit voluptate. Assumenda facere inventore perspiciatis! Assumenda deserunt id molestias perspiciatis repudiandae sequi velit!</p>
                <hr>
                <p><strong>Qualifications </strong></p>
                <ul class="list-unstyled">
                    <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae blanditiis culpa ducimus fugiat illo iste laborum.</li>
                    <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae blanditiis culpa ducimus fugiat illo iste laborum.</li>
                    <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae blanditiis culpa ducimus fugiat illo iste laborum.</li>
                    <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae blanditiis culpa ducimus fugiat illo iste laborum.</li>
                </ul>
                <hr>
                <p><strong>Benifits</strong></p>
                <ul class="list-unstyled">
                    <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae blanditiis culpa ducimus fugiat illo iste laborum.</li>
                    <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae blanditiis culpa ducimus fugiat illo iste laborum.</li>
                    <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae blanditiis culpa ducimus fugiat illo iste laborum.</li>
                    <li>- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae blanditiis culpa ducimus fugiat illo iste laborum.</li>
                </ul>
            </div>
            <hr>
            <p><strong>Deadline to apply</strong></p>
            <p><i>12th July 2018</i></p>
            <hr>
            <div class="buttons">
                <a href="" class="btn btn-black">Apply Now</a>
            </div>
        </div>
    </div>
</section>
@endsection
