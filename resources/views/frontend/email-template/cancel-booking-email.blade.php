@extends('frontend.layouts.email-layout')
@section('content')

<table class="bg-main">
    <tr>
        <td>
            <table class="container">
                <tr>
                    <td class="section">

                        <table class="table-details">

                            <tbody>
                                <tr>
                                    <td colspan="2">

                                            <p> Dear {{$data['data'][0]['first_name']}} {{$data['data'][0]['last_name']}},</p> <br/>
                                            <p>Your Booking has been cancelled. Please contact us for further detail.
                                            Thankyou!
                                            </p>
                                    </td>
                                </tr>

                            </tbody>
                        </table>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


@endsection