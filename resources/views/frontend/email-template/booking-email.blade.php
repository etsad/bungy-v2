@extends('frontend.layouts.email-layout')
@section('content')

<table class="bg-main">
    <tr>
        <td>
            <table class="container">
                <tr>
                    <td class="section">

                        <table class="table-details">

                            <tbody>
                            @if (isset($data['data']['recipient']) && $data['data']['recipient'] == 'Admin')
                            <tr>
                                <td colspan="2">

                                    <p> <strong>Dear Admin</strong><br>
                                        Following are the online booking details. </p>
                                </td>
                            </tr>
                            @else
                                <tr>
                                    <td colspan="2">

                                            <p> Dear {{$data['bookingData'][0]['first_name']}} {{$data['bookingData'][0]['last_name']}},</p> <br/>
                                            <p>Thank you for your booking.We have received your booking details.We'll
                                                confirm your booking shortly.</p>
                                        @endif
                                    </td>
                                </tr>
                                @php($x=1)
                            @foreach($data['bookingData'] as $bookingData)
                            <tr>
                                <th colspan="2" class="table-details-title">Jumpers Details - Jumper({{$x++}})</th>
                            </tr>
                            @if(isset($bookingData->ref_id))
                                <tr>
                                    <th width="30%">Booked RefID</th>
                                    <td>{{$bookingData->ref_id}}</td>
                                </tr>
                            @endif
                            @if(isset($bookingData->first_name)&&isset($bookingData->last_name))
                                <tr>
                                    <th width="30%">Name</th>
                                    <td>{{$bookingData->first_name}} {{$bookingData->last_name}}</td>
                                </tr>
                            @endif
                            @if(isset($data['data']['email']))
                                <tr>
                                    <th>Email</th>
                                    <td>{{$data['data']['email']}}</td>
                                </tr>
                            @endif
                            @if(isset($data['data']['phone']))
                                <tr>
                                    <th>Cell No.</th>
                                    <td>+{{$data['data']['phone']}}</td>
                                </tr>
                            @endif
                            @if(isset($bookingData->user_type))
                                <tr>
                                    <th>Nationality Type</th>
                                    <td>{{$bookingData->user_type}}</td>
                                </tr>
                            @endif
                                @if(isset($bookingData->citizenship_number))
                                    <tr>
                                        <th>Citizenship Number</th>
                                        <td>{{$bookingData->citizenship_number}}</td>
                                    </tr>
                                @endif
                                @if ($data['data']['recipient'] == 'Admin' && $bookingData->citizenship_front)
                                    <tr>
                                        <th>Citizenship Picture</th>
                                        <td><img src="/uploads/citizenship/"{{$bookingData->citizenship_front}}></td>
                                    </tr>
                                @endif
                                    <tr>
                                <th colspan="2" class="table-details-title">Bungy Details</th>
                            </tr>
                            @if(isset($bookingData->jump_date))
                                <tr>
                                    <th width="30%">Date</th>
                                    <td>{{$bookingData->jump_date}}</td>
                                </tr>
                            @endif
                            @if(isset($bookingData->quantity))
                                <tr>
                                    <th>No. of Customer</th>
                                    <td>{{$bookingData->quantity}}</td>
                                </tr>
                            @endif
                            @endforeach
                                @if(isset($data['total']))
                                <tr>
                                <th>Final Price</th>
                                <td>{{$data['total']}}</td>
                            </tr>
                                @endif

                            </tbody>
                        </table>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


@endsection