@extends('frontend.layouts.layout')
@section('content')
    <section class="inner-banner works">

    </section>
    <section class="work-story">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading-text">
                        <h4>July - December 2018</h4>
                    </div>
                    <div class="work-new-slider">
                        <div class="works-new">
                            <div class="work-box">
                                <img src="https://via.placeholder.com/300x200" alt="" class="img-responsive">
                                <br>
                                <div class="heading-text">
                                    <p><strong>Work Title Will Be Wriiten Here</strong></p>
                                </div>
                                <a href="/work-inner">Know More</a>
                            </div>
                        </div>
                        <div class="works-new">
                            <div class="work-box">
                                <img src="https://via.placeholder.com/300x200" alt="" class="img-responsive">
                                <br>
                                <div class="heading-text">
                                    <p><strong>Work Title Will Be Wriiten Here</strong></p>
                                </div>
                                <a href="/work-inner">Know More</a>
                            </div>
                        </div>
                        <div class="works-new">
                            <div class="work-box">
                                <img src="https://via.placeholder.com/300x200" alt="" class="img-responsive">
                                <br>
                                <div class="heading-text">
                                    <p><strong>Work Title Will Be Wriiten Here</strong></p>
                                </div>
                                <a href="/work-inner">Know More</a>
                            </div>
                        </div>
                        <div class="works-new">
                            <div class="work-box">
                                <img src="https://via.placeholder.com/300x200" alt="" class="img-responsive">
                                <br>
                                <div class="heading-text">
                                    <p><strong>Work Title Will Be Wriiten Here</strong></p>
                                </div>
                                <a href="/work-inner">Know More</a>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="heading-text">
                        <h4>January - June 2018</h4>
                    </div>
                    <div class="work-new-slider">
                        <div class="works-new">
                            <div class="work-box">
                                <img src="https://via.placeholder.com/300x200" alt="" class="img-responsive">
                                <br>
                                <div class="heading-text">
                                    <p><strong>Work Title Will Be Wriiten Here</strong></p>
                                </div>
                                <a href="/work-inner">Know More</a>
                            </div>
                        </div>
                        <div class="works-new">
                            <div class="work-box">
                                <img src="https://via.placeholder.com/300x200" alt="" class="img-responsive">
                                <br>
                                <div class="heading-text">
                                    <p><strong>Work Title Will Be Wriiten Here</strong></p>
                                </div>
                                <a href="/work-inner">Know More</a>
                            </div>
                        </div>
                        <div class="works-new">
                            <div class="work-box">
                                <img src="https://via.placeholder.com/300x200" alt="" class="img-responsive">
                                <br>
                                <div class="heading-text">
                                    <p><strong>Work Title Will Be Wriiten Here</strong></p>
                                </div>
                                <a href="/work-inner">Know More</a>
                            </div>
                        </div>
                        <div class="works-new">
                            <div class="work-box">
                                <img src="https://via.placeholder.com/300x200" alt="" class="img-responsive">
                                <br>
                                <div class="heading-text">
                                    <p><strong>Work Title Will Be Wriiten Here</strong></p>
                                </div>
                                <a href="/work-inner">Know More</a>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="heading-text">
                        <h4>July - December 2017</h4>
                    </div>
                    <div class="work-new-slider">
                        <div class="works-new">
                            <div class="work-box">
                                <img src="https://via.placeholder.com/300x200" alt="" class="img-responsive">
                                <br>
                                <div class="heading-text">
                                    <p><strong>Work Title Will Be Wriiten Here</strong></p>
                                </div>
                                <a href="/work-inner">Know More</a>
                            </div>
                        </div>
                        <div class="works-new">
                            <div class="work-box">
                                <img src="https://via.placeholder.com/300x200" alt="" class="img-responsive">
                                <br>
                                <div class="heading-text">
                                    <p><strong>Work Title Will Be Wriiten Here</strong></p>
                                </div>
                                <a href="/work-inner">Know More</a>
                            </div>
                        </div>
                        <div class="works-new">
                            <div class="work-box">
                                <img src="https://via.placeholder.com/300x200" alt="" class="img-responsive">
                                <br>
                                <div class="heading-text">
                                    <p><strong>Work Title Will Be Wriiten Here</strong></p>
                                </div>
                                <a href="/work-inner">Know More</a>
                            </div>
                        </div>
                        <div class="works-new">
                            <div class="work-box">
                                <img src="https://via.placeholder.com/300x200" alt="" class="img-responsive">
                                <br>
                                <div class="heading-text">
                                    <p><strong>Work Title Will Be Wriiten Here</strong></p>
                                </div>
                                <a href="/work-inner">Know More</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>
@endsection
