@if (Session::has('flash'))
    <div class="alert alert-{{ Session::get('flash')['type'] }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ Session::get('flash')['message'] }}
    </div>
@endif

