<?php

namespace App\Services;

use App\Booking;
use App\Price;
use App\User;
use DB;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Image;

class UserService
{
    public function __construct(User $user, MailService $mailService)
    {
        $this->user = $user;
        $this->mail = $mailService;
    }

    public function saveUpdate($userData, $id = null)
    {
        DB::beginTransaction();
        try {
            $this->user->create($userData);
            DB::commit();

        } catch (\Exception $exception) {
            dd($exception);
            DB::rollBack();
            return $exception->getMessage();
        }
    }


    public function updateUser($userData)
    {
        try {
                $updateuser = $this->user->where(['id' => $userData['id']])->update($userData);
                return $updateuser;
        }catch (\Exception $e){
            dd($e);
        }
    }

    public function getByUserId($id)
    {
        $user = $this->user->find($id);
        return $user;
    }

    public function delete($id)
    {
        $deleteUser = $this->user->where('id',$id)->delete();
        return $deleteUser;
    }

    public function activate($userId)
    {
        $activateUser =  $this->user->where(['id'=>$userId])->update(['status'=>'Active']);
        return $activateUser;
    }

    public function deactivate($userId)
    {
        $deactivateUser =  $this->user->where(['id'=>$userId])->update(['status'=>'Inactive']);
        return $deactivateUser;
    }




    public function getAllUsers($page=null)
    {
        $userList = $this->user->orderBy('id','ASC')->paginate($page);
        return $userList;
    }

}