<?php
namespace App\Services;
use App\Libraries\TMHelper;
use Mail;
use Pelago\Emogrifier;

class MailService
{

    private function emogrify($path, $data)
    {
        $emogrifier = new Emogrifier();
        $emogrifier->setHtml(view($path, compact('data')));
        $emogrifier->setCss(file_get_contents(app_path() . '../../public/emailer/newcss.css'));
        $emogrifier->setCss(file_get_contents(app_path() . '../../public/emailer/email-template.css'));
        $contactHtml = $emogrifier->emogrify();

        return $contactHtml;
    }


    public function sendContactEmail($data)
    {
        $html = $this->emogrify('frontend.email-template.contact-email', $data);

        return Mail::send(
            [],
            [],
            function ($message) use ($data, $html) {
                $message->from(env('SITE_EMAIL'), env('SITE_NAME'));
                $message->to(env('ADMIN_EMAIL'));
                $message->subject('Contact Email Received');
                $message->setBody($html, 'text/html');
            }
        );
    }

    public function sendBookingEmail($data, $bookingData , $total)
    {
        $data['recipient'] = 'Admin';
        $emaileTemplate =  $this->emogrify('frontend.email-template.booking-email', compact('bookingData','data','total'));
        $this->sendEmail(
            $emaileTemplate,
            $receiver = env('ADMIN_EMAIL'),
            $subject = "Reservation received - " .env('SITE_NAME')
        );
        $data['recipient'] = 'Customer';
        $emaileTemplate =  $this->emogrify('frontend.email-template.booking-email', compact('bookingData','data','total'));
        $this->sendEmail(
            $emaileTemplate,
            $receiver = $data['email'],
            $subject = "Confirm Reservation - " .env('SITE_NAME')
        );


    }

    public function sendBookingCancelledEmail($data)
    {
        $emailTemplate =  $this->emogrify('frontend.email-template.cancel-booking-email', compact('data'));
        $this->sendEmail(
            $emailTemplate,
            $receiver = $data['email'],
            $subject = "Cancelled Reservation - " .env('SITE_NAME')
        );
    }


    public function sendLoginEmail($data)
    {
        $html = $this->emogrify('frontend.email-template.userinfo-email', $data);

        return Mail::send(
            [],
            [],
            function ($message) use ($data, $html) {
                $message->from(env('SITE_EMAIL'), env('SITE_NAME'));
                $message->to($data['email']);
                $message->subject('Login Confirmation Email Received');
                $message->setBody($html, 'text/html');
            }
        );
    }

    public function sendEmail($body, $receiver, $subject)
    {
        try {
            Mail::send([], [], function ($message) use ($receiver, $body, $subject) {
                $message->from(env('SITE_EMAIL'), env('SITE_NAME'));
                $message->to($receiver);
                $message->subject($subject);
                $message->setBody($body, 'text/html');
            });
        } catch (\exception $e) {
            return false;
        }

        return true;
    }
}


