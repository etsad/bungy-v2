<?php

namespace App\Services;

use App\News;
use Image;

class NewsService
{

    private $news;

    public function __construct(News $news)
    {
        $this->news = $news;
    }
    public function saveUpdate($data,$id=null)
    {
        try {
            unset($data['_token']);
            if (isset($data['news_image']) && $data['news_image'] !== null) {
                $image = $data['news_image'];
                $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();

                $destinationPath = public_path('/uploads/news/thumbnail');
                $img = Image::make($image->getRealPath());
                $img->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $input['imagename']);


                $destinationPath = public_path('/uploads/news/');
                $image->move($destinationPath, $input['imagename']);
                $data['news_image'] = $input['imagename'];
            } else {
                unset($data['news_image']);
            }
            if ($id !== null) {
                $savenews = $this->news->where('ID', $id)->update($data);
                return $savenews;
            }
            $savenews = $this->news->create($data);
            return $savenews;

        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }


    public function getAllNews()
    {
        $news = $this->news->orderBy('ID','DESC')->get();
        return $news;
    }

    public function getByID($id)
    {
        $news = $this->news->find($id);
        return $news;
    }

    public function delete($id)
    {
        $deleteNews = $this->news->where('ID',$id)->delete();
        return $deleteNews;
    }




}