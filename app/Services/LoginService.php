<?php
/**
 * Created by PhpStorm.
 * User: Nepsrock
 * Date: 2018-03-03
 * Time: 5:48 PM
 */

namespace App\Services;
use App\Booking;
use App\Libraries\TMHelper;
use App\UserRoles;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Services\Helper;




class LoginService
{

    private  $user;
    private  $userRoles;
    private $mail;
    private $booking;

    public function __construct(User $user,UserRoles $userRoles, MailService $mailService,Booking $booking)
    {
        $this->user = $user;
        $this->userRoles = $userRoles;
        $this->mail = $mailService;
        $this->booking = $booking;
    }

    public function authenticateUser($data)
    {

        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            $user = Auth::user();
            return $user;
        }
        return false;
    }

    public function checkAdmin($data)
    {
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            $user = Auth::id();
            $userRoles = $this->userRoles->getUserRoles($user);
            Session::put('userRoles',$userRoles);
            return $userRoles;
        }

    }

    public function registerUser($data)
    {
         Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $user = $this->user->create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        return compact('user');

    }

    public function logout()
    {
        session_start();
        Auth::logout();
        session_destroy();
    }

    public function deleteAccount($userId)
    {
        try{
            $this->user->where(['id'=>$userId])->delete();
            $this->booking->where(['user_id'=>$userId])->delete();
            $this->logout();
        }
        catch (\Exception $e){
            dd($e);
        }
    }


}