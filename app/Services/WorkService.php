<?php

namespace App\Services;

use App\Work;
use Image;

class WorkService
{

    private $work;

    public function __construct(Work $work)
    {
        $this->work = $work;
    }
    public function saveUpdate($data,$id=null)
    {
        try {
            unset($data['_token']);
            if (isset($data['work_image']) && $data['work_image'] !== null) {
                $image = $data['work_image'];
                $input['imagename'] = time() . '.' . $image->getClientOriginalExtension();

                $destinationPath = public_path('/uploads/work/thumbnail');
                $img = Image::make($image->getRealPath());
                $img->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $input['imagename']);


                $destinationPath = public_path('/uploads/work/');
                $image->move($destinationPath, $input['imagename']);
                $data['work_image'] = $input['imagename'];
            } else {
                unset($data['work_image']);
            }
            if ($id !== null) {
                $savework = $this->work->where('ID', $id)->update($data);
                return $savework;
            }
            $savework = $this->work->create($data);
            return $savework;

        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }


    public function getAllWork()
    {
        $work = $this->work->orderBy('ID','DESC')->get();
        return $work;
    }

    public function getByID($id)
    {
        $work = $this->work->find($id);
        return $work;
    }

    public function delete($id)
    {
        $deleteWork = $this->work->where('ID',$id)->delete();
        return $deleteWork;
    }




}