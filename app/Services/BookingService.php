<?php

namespace App\Services;

use App\Booking;
use App\Price;
use DB;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Image;

class BookingService
{
    public function __construct(Booking $booking, MailService $mailService, Price $price)
    {
        $this->booking = $booking;
        $this->mail = $mailService;
        $this->price = $price;
    }

    public function saveUpdate($bookingData, $id = null)
    {
        DB::beginTransaction();
        $userId = null;
        if (Auth::user()){
            $userId =  Auth::user()->id;
        }
        try {
            unset($bookingData['_token']);
            $citizenshipImage = isset($bookingData['citizenship_front']) ? $bookingData['citizenship_front'] : '';
            for ($i = 0; $i < count(array_filter($bookingData['first_name'])); $i++) {
                if (isset($citizenshipImage[$i]) && $citizenshipImage[$i] !== null) {
                    $image[] = $citizenshipImage[$i];
                    $input['imagename'][$i] = time() . '.' . $image[$i]->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/citizenship/');
                    $image[$i]->move($destinationPath, $input['imagename'][$i]);
                    $citizenshipImage[$i] = $input['imagename'][$i];
                    $new_booking_data = array(
                        "jump_date" => $bookingData['jump_date'],
                        "quantity" => $bookingData['quantity'],
                        "user_type" => $bookingData['user_type'][$i],
                        "citizenship_number" => $bookingData['citizenship_number'][$i],
                        "first_name" => $bookingData['first_name'][$i],
                        "last_name" => $bookingData['last_name'][$i],
                        "gender" => $bookingData['gender'][$i],
                        "age" => $bookingData['age'][$i],
                        "blood_group" => $bookingData['blood_group'][$i],
                        "citizenship_front" => $citizenshipImage[$i],
                        "user_id" => $userId
                    );
                } else {
                    unset($citizenshipImage[$i]);
                }

                    $new_booking_data['ref_id'] = $this->refIdGenerate();
                    $savebooking[] = $this->booking->create($new_booking_data);
                    DB::commit();

            }
            return $savebooking;


        } catch (\Exception $exception) {
            dd($exception);
            DB::rollBack();
            return $exception->getMessage();
        }
    }

    public function updateEmailPhone($bookingData,$bookingId)
    {
        try {
            foreach ($bookingId as $id) {
                $savebooking = $this->booking->where(['id' => $id])->update($bookingData);
            }
            return $savebooking;
        }catch (\Exception $e){
            dd($e);
        }
    }

    public function updateBooking($bookingData)
    {
        try {
                $updatebooking = $this->booking->where(['id' => $bookingData['id']])->update($bookingData);
                return $updatebooking;
        }catch (\Exception $e){
            dd($e);
        }
    }


    public function refIdGenerate()
    {
        $ref_id = rand(10000, 99999);
        $booking = Booking::where('ref_id', $ref_id)->first();
        if (!$booking) {
            return $ref_id;
        } else {
            refIdGenerate();
        }
    }
    public function getByBookingId($id)
    {
        $booking = $this->booking->find($id);
        return $booking;
    }

    public function getByBookingRefId($ref_id)
    {
        $booking = $this->booking->where(['ref_id'=>$ref_id])->first();
        return $booking;
    }

    public function delete($id)
    {
        $deleteBooking = $this->booking->where('id',$id)->delete();
        return $deleteBooking;
    }

    public function getPriceByUserType($userType)
    {
        $discount = 0;
        $definedPrice = $this->price->first();
        $price = $definedPrice['price'];
        if ($userType !== 'Foreigner') {
            $discount = 15;
            $price -= $price * $discount / 100;
        }

        return compact('discount', 'price');
    }

    public function getAllActiveBookingByUserId()
    {
        $activeBookingList = $this->booking->where('user_id',Auth::user()->id)
          ->where('jump_date','>',now())
           ->where('status','=','Pending')
           ->Orwhere('status','=','Approved')
           ->get();
        return $activeBookingList;
    }

    public function getAllCompletedBookingByUserId()
    {
        $CompletedBookingList = $this->booking->where('user_id',Auth::user()->id)
            ->where('jump_date','<',now())
            ->where('status','=','Completed')->get();
        return $CompletedBookingList;
    }

    public function getAllBookings($page=null)
    {
        $bookingList = $this->booking->orderBy('id','DESC')->paginate($page);
        return $bookingList;
    }

    public function cancelBooking($bookingRefId)
    {
       $cancelBooking =  $this->booking->where(['ref_id'=>$bookingRefId])->update(['status'=>'Cancel']);
        return $cancelBooking;
    }


    public function complete($bookingRefId)
    {
        $completeBooking =  $this->booking->where(['ref_id'=>$bookingRefId])->update(['status'=>'Completed']);
        return $completeBooking;
    }

    public function approve($bookingRefId)
    {
        $approveBooking =  $this->booking->where(['ref_id'=>$bookingRefId])->update(['status'=>'Approved']);
        return $approveBooking;
    }




}