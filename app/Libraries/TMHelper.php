<?php

namespace App\Libraries;

use App\Models\File;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class TMHelper
{

    public static function flash($type, $message)
    {
        Session::flash(
            'flash',
            [
                'type'    => $type,
                'message' => $message
            ]
        );
    }

    public static function apiResponse($status, $message, $data = null)
    {
        return [
            'status'  => $status,
            'message' => $message,
            'data'    => $data
        ];
    }

    public static function upload(UploadedFile $file, $options = [])
    {

        $options         = [
            'visibility' => isset($options['visibility']) ? $options['visibility'] : 'public'
        ];
        $destinationPath = 'uploads/' . date('Y') . '/' . date('m');

        if ($options['visibility'] == 'public') {
            $destinationPath = 'public/' . $destinationPath;
        }

        Storage::makeDirectory($destinationPath);

        $file->store($destinationPath);

        Artisan::call('storage:link');

        return $file;
    }

    public static function imagePath(File $file = null)
    {

        if ($file == null) {
            return '';
        }

        $path = 'storage/uploads/';
        $path .= date('Y/m', strtotime($file->created_at));
        $path .= '/' . $file->name;

        return asset($path);
    }



}
