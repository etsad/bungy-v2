<?php

namespace App\Http\Controllers;

use App\Services\BookingService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PageController extends Controller
{
    public function __construct(BookingService $bookingService)
    {
        $this->booking = $bookingService;
    }

    public function getPage($slug = null)
    {
        switch ($slug) {
            case '':
                return view('frontend.index');
                break;
            case 'account':
                $activeBooking = $this->booking->getAllActiveBookingByUserId();
                $completedBooking = $this->booking->getAllCompletedBookingByUserId();
                return view('frontend.account',compact('activeBooking','completedBooking'));
                break;
            default:
                if (View::exists('frontend.'.$slug)) {
                    return view('frontend.'.$slug);
                }
        }

        return view('frontend.404');
    }
}
