<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use View;

class PageController extends Controller
{
    public function __construct()
    {
        if (!Auth::user()) {
            return redirect('admin/login');
            exit;
        }else{
            return redirect('admin/dashboard');
        }
    }

    public function getPage($slug = null)
    {
        switch ($slug) {
            case 'dashboard':
                return view('admin.index');
                break;
            default:
                if (View::exists('admin.'.$slug)) {
                    return view('admin.'.$slug);
                }
        }

        return view('frontend.404');
    }
}
