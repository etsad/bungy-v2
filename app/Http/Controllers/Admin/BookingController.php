<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\TMHelper;
use App\Services\BookingService;
use App\Services\MailService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;

class BookingController extends Controller
{
    public function __construct(BookingService $bookingService, MailService $mailService)
    {
        $this->booking = $bookingService;
        $this->mail = $mailService;
    }

    public function index()
    {

        $bookings = $this->booking->getAllBookings(1);
        return view('admin.booking',compact('bookings'));
    }

    public function editBooking($id)
    {
        if ($id !== null) {
            $booking = $this->booking->getByBookingId($id);
            return view('admin.edit-booking', compact('booking'));
        }
    }

    public function viewBooking($id)
    {
        if ($id !== null) {
            $booking = $this->booking->getByBookingId($id);
            return view('admin.view-booking', compact('booking'));
        }
    }

    public function updateBooking(Request $request)
    {

        $request->validate([
            'jump_date' => 'required',
            'quantity' => 'required|max:3',
            'user_type' => 'required',
            'citizenship_number' => 'required',
            'first_name' => 'required|max:25',
            'last_name' => 'required|max:25',
            'gender' => 'required',
            'age' => 'required|max:60',
            'blood_group' => 'required',
        ]);
        $data = $request->except('_token');
        if ($data['id'] !== null) {
            $this->booking->updateBooking($data);
            TMHelper::flash('success','Booking Id:'.$data['id'].' Updated Successfully.');
            return redirect('/admin/booking');
        }
    }

    public function deleteBooking($id)
    {
        $this->booking->delete($id);
        TMHelper::flash('success','Booking Id:'.$id. ' Deleted Successfully.');
        return redirect()->back();
    }

    public function approveBooking($ref_id)
    {
        $this->booking->approve($ref_id);
        TMHelper::flash('success','Booking Reference Id: #'.$ref_id. ' Approved Successfully.');
        return redirect()->back();
    }


    public function completeBooking($ref_id)
    {
        $this->booking->complete($ref_id);
        TMHelper::flash('success','Booking Reference Id: #'.$ref_id. ' Marked As Complete Successfully.');
        return redirect()->back();
    }


    public function cancelBooking($ref_id)
    {
        $this->booking->cancelBooking($ref_id);
        $data = $this->booking->getByBookingRefId($ref_id);
        $this->mail->sendBookingCancelledEmail($data);
        TMHelper::flash('success','Booking Reference Id: #'.$ref_id.' Cancelled Successfully.');
        return redirect()->back();
    }


}
