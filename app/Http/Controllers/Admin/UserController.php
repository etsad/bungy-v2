<?php

namespace App\Http\Controllers\Admin;

use App\Libraries\TMHelper;
use App\Services\BookingService;
use App\Services\MailService;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use View;

class UserController extends Controller
{
    public function __construct(UserService $userService, MailService $mailService)
    {
        $this->user = $userService;
        $this->mail = $mailService;
    }

    public function index()
    {

        $users = $this->user->getAllUsers(10);
        return view('admin.users',compact('users'));
    }

    public function profile()
    {
        $userId = Auth::user()->id;
        if ($userId !== null) {
            $user = $this->user->getByUserId($userId);
            return view('admin.profile', compact('user'));
        }
    }


    public function saveUpdateUser(Request $request)
    {
        $user = Auth::user();
        $validation = Validator::make($request->all(), [
            'password' => 'hash:' . $user->password,
            'new_password' => 'required'
        ]);


        if ($validation->fails()) {
            TMHelper::flash('error', 'Invalid password.');
            return redirect()->back()->withErrors($validation->errors());
        }

        if (Hash::check($request->current_password, $user->password)) {
            if ($request->new_password == $request->confirm_password) {
                $user->password = Hash::make($request->input('new_password'));
                $user->save();

                TMHelper::flash('success', 'Your new password is now set!');
                return redirect()->back();
            } else {
                TMHelper::flash('error', 'Your confirmation password do not match!');
                return redirect()->back();
            }
        }else{
            TMHelper::flash('error', 'Your old password do not match!');
            return redirect()->back();
        }

    }

    public function deleteUser($id)
    {
        $this->user->delete($id);
        return redirect()->back();
    }

    public function activateUser($id)
    {
        $this->user->activate($id);
        return redirect()->back();
    }


    public function deactivateUser($id)
    {
        $this->user->deactivate($id);
        return redirect()->back();
    }

}
