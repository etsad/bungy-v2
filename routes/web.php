<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin'], function () {
    Route::get('/filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
    Route::post('/filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');
});

Route::group(['prefix' => 'admin/booking'], function () {
    Route::get('/','Admin\BookingController@index');
    Route::get('/{id}/edit','Admin\BookingController@editBooking');
    Route::get('/{id}/view','Admin\BookingController@viewBooking');
    Route::get('/{id}/delete','Admin\BookingController@deleteBooking');
    Route::post('/update','Admin\BookingController@updateBooking');
    Route::get('/{id}/approve','Admin\BookingController@approveBooking');
    Route::get('/{id}/completed','Admin\BookingController@completeBooking');
    Route::get('/{id}/cancel','Admin\BookingController@cancelBooking');
});

Route::group(['prefix' => 'admin/users'], function () {
    Route::get('/profile','Admin\UserController@profile');
    Route::post('/add','Admin\UserController@saveUpdateUser');
    Route::get('/{id}/edit','Admin\UserController@editUser');
    Route::get('/{id}/delete','Admin\UserController@deleteUser');
    Route::post('/update','Admin\UserController@saveUpdateUser');
    Route::get('/{id}/active','Admin\UserController@activateUser');
    Route::get('/{id}/inactive','Admin\UserController@deactivateUser');
});

Route::group(['prefix' => 'admin'], function () {
    Route::post('/login-submit','Auth\LoginController@adminLogin');
    Route::get('/logout','Auth\LoginController@adminLogout');
    Route::get('/{slug}','Admin\PageController@getPage');
});














Route::post('/register','Auth\LoginController@postRegister');
Route::post('/authenticate','Auth\LoginController@Authenticate');
Route::post('/change-password','Auth\LoginController@changePassword');
Route::get('/logout','Auth\LoginController@logout');
Route::post('/delete-account','Auth\LoginController@deleteAccount');

Route::group(['prefix' => 'bookings/'], function () {
    Route::post('/quote','BookingController@postBookingDate');
    Route::get('/user-info','BookingController@getUserInfoForm');
    Route::post('/user-info','BookingController@postUserInfo');
    Route::get('/confirm','BookingController@getConfirmation');
    Route::post('/thankyou','BookingController@postEmailConfirmation');
    Route::get('/{id}/cancel','BookingController@cancelBooking');
});




Route::get('/', function () {
    return view('frontend.index');
});
Route::get('/{slug}','PageController@getPage');
